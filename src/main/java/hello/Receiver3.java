package hello;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Receiver3 {


    @JmsListener(destination = "mailbox.topic", containerFactory = "topicListenerContainerFactory")
    public void receiveMessageFromTopic3(Email email) {
        System.out.println("Received from topic 3 <" + email + ">");
    }

}

package hello;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Receiver4 {

    @JmsListener(destination = "mailbox.topic", containerFactory = "topicListenerContainerFactory")
    public void receiveMessageFromTopic4(Email email) {
        System.out.println("Received from topic 4 <" + email + ">");
    }

}

package hello;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Receiver2 {


    @JmsListener(destination = "mailbox.topic", containerFactory = "topicListenerContainerFactory")
    public void receiveMessageFromTopic2(Email email) {
        System.out.println("Received from topic 2 <" + email + ">");
    }

}

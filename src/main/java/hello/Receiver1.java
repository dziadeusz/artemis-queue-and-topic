package hello;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Receiver1 {

    public Receiver1() {
        int a =1;
    }

    @JmsListener(destination = "mailbox.queue", containerFactory = "queueListenerContainerFactory")
    public void receiveMessageFromTopic1(Email email) {
        System.out.println("Received from queue<" + email + ">");
    }


}

package hello;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.concurrent.CountDownLatch;


/**
 * Created by kkuc on 19.05.17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class, Receiver1Test.class})
@ActiveProfiles("integration")
@Configuration
public class Receiver1Test {

	@Autowired
	private Receiver1 receiver1;

	@Autowired
	private JmsTemplate jmsTemplate;

	@Autowired
	private CountDownLatch countDownLatch;
	private Email result;

	@Test
	public void receiveMessageFromTopic1() throws Exception {
		//given
		Email email = new Email("test1", "test2");
		//when
		jmsTemplate.convertAndSend("mailbox.queue", email);
		//then
		countDownLatch.await();
		Assertions.assertThat(email).isEqualToComparingFieldByField(result);
	}

	@Bean
	CountDownLatch countDownLatch(){
		return new CountDownLatch(1);
	}

	@Bean
	public JmsListenerInterceptor jmsListenerInterceptor(){
		return new JmsListenerInterceptor();
	}


	@Aspect
	public class JmsListenerInterceptor {

		@Autowired
		private CountDownLatch countDownLatch;

		@After("execution(*  hello.Receiver1.receiveMessageFromTopic1(..))")
		public void afterOnMessage(JoinPoint jp) {
			Object[] args = jp.getArgs();
			Receiver1Test.this.result = (Email) args[0];
			countDownLatch.countDown();
		}

	}


}